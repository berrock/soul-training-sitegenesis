/**
* A hello world controller.  This file is in cartridge/controllers folder
*
**
* @module controllers/JHelloWorldV1          
*/

var guard = require('app_storefront_controllers/cartridge/scripts/guard');
var ISML = require('dw/template/ISML');

function start() {
	    
	    ISML.renderTemplate(
	                          'helloworld3.isml', {myParameteronISML:"Hello 222 from Controllers"}
	                         );
	  
};
exports.Start = guard.ensure(['get'], start);
